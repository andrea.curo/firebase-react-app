import React, { useState, useEffect } from "react";
import { Typography, Card, CardContent } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import FirebaseService from "../services/firebaseService";

import ItemCreation from "../Creation/ItemCreation";

export default function ListWrapper(props) {
  const firebaseService = FirebaseService();
  const [user, setUser] = useState({});
  const [myList, setMyList] = useState([]);
  const [showItemCreation, setShowItemCreation] = useState(false);
  const [isCreate, setIsCreate] = useState(false);

  function closeItemCreation() {
    setShowItemCreation(false);
  }

  useEffect(() => {
    setUser(props.user);

    async function fetchLists() {
      const myList = await firebaseService.getListById(props.user.id);
      setMyList(myList);
      setIsCreate(myList.length === 0);
    }

    fetchLists();
  }, [props.user, showItemCreation]);

  if (!showItemCreation) {
    return (
      <div className={props.classes.selectionPage}>
        <Typography variant="h6">{user.name}'s list</Typography>
        <div className={props.classes.cardWrapper}>
          {myList.map((item) => {
            let name = <p>{item.name}</p>;
            if (item.url && typeof item.url === "string") {
              name = <a href={item.url}>{item.name}</a>;
            }
            return (
              <Card className={props.classes.card} key={item.id}>
                <CardContent>{name}</CardContent>
              </Card>
            );
          })}
          <div onClick={setShowItemCreation}>
            <Card className={props.classes.card}>
              <CardContent>
                <AddIcon />
              </CardContent>
            </Card>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <ItemCreation
        classes={props.classes}
        isCreate={isCreate}
        id={props.user.id}
        close={closeItemCreation}
      />
    );
  }
}
