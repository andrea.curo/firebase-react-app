import FirebaseDB from "./firebaseDB";

const FirebaseService = () => {
  const firebaseDB = FirebaseDB();

  const createUser = (user, successCallback, errorCallback) => {
    const callback = (e) => {
      if (e) {
        errorCallback(e);
      } else {
        successCallback();
      }
    };

    firebaseDB.createUser(
      {
        name: user,
      },
      callback
    );
  };

  const getUsers = async () => {
    const userData = await firebaseDB.getUsers();
    let userArr = [];
    Object.keys(userData).forEach((key) => {
      userArr.push({
        name: userData[key].name,
        id: key,
      });
    });

    return userArr;
  };

  const getLists = async () => {
    const listData = await firebaseDB.getLists();
    let lists = {};
    Object.keys(listData).forEach((key) => {
      let itemList = [];
      Object.keys(listData[key]).forEach((itemKey) => {
        itemList.push({
          name: listData[key][itemKey].name,
          id: itemKey,
          url: listData[key][itemKey].url,
        });
      });
      lists[key] = itemList;
    });
    console.log(lists);
    return lists;
  };

  const getListById = async (id) => {
    const listData = await firebaseDB.getListById(id);
    let itemList = [];

    if (listData && Object.keys(listData) && Object.keys(listData).length > 0) {
      Object.keys(listData).forEach((itemKey) => {
        itemList.push({
          name: listData[itemKey].name,
          id: itemKey,
          url: listData[itemKey].url,
        });
      });
    }
    return itemList;
  };

  const updateList = async (id, item, successCallback, errorCallback) => {
    const callback = (e) => {
      if (e) {
        errorCallback(e);
      } else {
        successCallback();
      }
    };

    await firebaseDB.updateList(id, item, callback);
  };

  return {
    createUser,
    getUsers,
    getLists,
    getListById,
    updateList,
  };
};

export default FirebaseService;
