import firebase from "firebase";

const API_KEY = process.env.REACT_APP_API_KEY || "";
const AUTH_DOMAIN = process.env.REACT_APP_AUTH_DOMAIN || "";
const DATABASE_URL = process.env.REACT_APP_DATABASE_URL || "";

const LIST_DOC = "lists";
const USER_DOC = "users";

// TODO: do i have to do awaits in this?
const FirebaseDB = () => {
  const init = () => {
    if (!firebase.apps.length) {
      var config = {
        apiKey: API_KEY,
        authDomain: AUTH_DOMAIN,
        databaseURL: DATABASE_URL,
      };
      firebase.initializeApp(config);
    }
  };

  const _getDatabase = () => {
    return firebase.database();
  };

  const getUsers = async () => {
    init();

    return await _getDatabase()
      .ref()
      .child(USER_DOC)
      .orderByKey()
      .once("value")
      .then((data) => {
        return data.val();
      });
  };

  const createUser = (user, callback) => {
    init();

    _getDatabase().ref().child(USER_DOC).push(user, callback);
  };

  const getLists = async () => {
    init();

    return await _getDatabase()
      .ref()
      .child(LIST_DOC)
      .orderByKey()
      .once("value")
      .then((data) => {
        return data.val();
      });
  };

  const getListById = async (id) => {
    init();

    return await _getDatabase()
      .ref()
      .child(`${LIST_DOC}/${id}`)
      .once("value")
      .then((data) => {
        return data.val();
      });
  };

  const updateList = async (id, item, callback) => {
    init();
    return await _getDatabase()
      .ref()
      .child(`${LIST_DOC}/${id}`)
      .push()
      .set(item, callback);
  };

  return {
    createUser,
    getUsers,
    getLists,
    getListById,
    updateList,
  };
};

export default FirebaseDB;
