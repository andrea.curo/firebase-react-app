import React, { useState } from "react";

import FirebaseService from "../services/firebaseService";
import { Typography, TextField, IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";

export default function Creation(props) {
  const firebaseService = FirebaseService();
  const [name, setName] = useState("");
  const [error, setError] = useState({
    status: false,
    msg: "",
  });

  function createUser() {
    firebaseService.createUser(name, props.close, errorCallback);
  }

  function errorCallback(e) {
    console.log(e);
    setError({
      status: true,
      msg: "Try Again",
    });
  }

  function handleChange(e) {
    setName(e.target.value);
  }

  return (
    <div className={props.classes.creationPage}>
      <Typography variant="h4">What is your name?</Typography>
      <form>
        <TextField
          error={error.status}
          helperText={error.msg}
          id="list-owner"
          value={name}
          onChange={handleChange}
        ></TextField>
        <IconButton aria-label="create" onClick={createUser}>
          <AddIcon />
        </IconButton>
      </form>
    </div>
  );
}
