import React, { useState } from "react";

import FirebaseService from "../services/firebaseService";
import { Typography, TextField, IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";

export default function Creation(props) {
  const firebaseService = FirebaseService();
  const [item, setItem] = useState({});
  const [error, setError] = useState({
    status: false,
    msg: "",
  });

  function updateList() {
    firebaseService.updateList(props.id, item, props.close, errorCallback);
  }

  function errorCallback(e) {
    console.log(e);
    setError({
      status: true,
      msg: "Try Again",
    });
  }

  function handleChange(e, type) {
    let tmpItem = Object.assign({}, item);
    tmpItem[type] = e.target.value;
    setItem(tmpItem);
  }

  return (
    <div className={props.classes.creationPage}>
      <Typography variant="h4">What will you add?</Typography>
      <form>
        <TextField
          error={error.status}
          helperText={error.msg}
          id="item-name"
          value={item.name}
          onChange={(e) => handleChange(e, "name")}
        ></TextField>
        <TextField
          error={error.status}
          helperText={error.msg}
          id="item-url"
          value={item.url}
          onChange={(e) => handleChange(e, "url")}
        ></TextField>
        <IconButton aria-label="create" onClick={updateList}>
          <AddIcon />
        </IconButton>
      </form>
    </div>
  );
}
