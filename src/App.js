import React, { useState, useEffect } from "react";

import { Card, CardContent, makeStyles, Typography } from "@material-ui/core";

import Creation from "./Creation/Creation";
import AddIcon from "@material-ui/icons/Add";
import FirebaseService from "./services/firebaseService";
import ListWrapper from "./List/ListWrapper";

const useClasses = makeStyles({
  selectionPage: {
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  listPage: {},
  creationPage: {
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  cardWrapper: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
    justifyContent: "center",
  },
  card: {
    width: "150px",
    height: "150px",
    margin: "20px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default function App() {
  const classes = useClasses();
  const firebaseService = FirebaseService();
  const [showList, setShowList] = useState(false);
  const [showCreation, setShowCreation] = useState(false);
  const [userList, setUserList] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);

  function closeCreation() {
    setShowCreation(false);
  }

  function handleShowList(user) {
    setSelectedUser(user);
    setShowList(true);
  }

  useEffect(() => {
    async function fetchUsers() {
      const users = await firebaseService.getUsers();
      setUserList(users);
    }

    fetchUsers();
  }, [showCreation]);

  if (!showList && !showCreation) {
    return (
      <div className={classes.selectionPage}>
        <Typography variant="h4">Select a list</Typography>
        <div className={classes.cardWrapper}>
          {userList.map((user, index) => {
            return (
              <div key={index} onClick={() => handleShowList(user)}>
                <Card className={classes.card}>
                  <CardContent>{user.name}</CardContent>
                </Card>
              </div>
            );
          })}
          <div onClick={setShowCreation}>
            <Card className={classes.card}>
              <CardContent>
                <AddIcon />
              </CardContent>
            </Card>
          </div>
        </div>
      </div>
    );
  } else if (showCreation) {
    return <Creation classes={classes} close={closeCreation}></Creation>;
  } else {
    return (
      <ListWrapper
        classes={classes}
        user={selectedUser}
        userList={userList}
      ></ListWrapper>
    );
  }
}
