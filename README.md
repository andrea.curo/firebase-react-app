# Wish List Maker Application

Wish list maker app built with Firebase and React. Firebase for hosting and realtime db; React for everything else!

## Tools

- React
- Material
- Firebase
- Lodash
